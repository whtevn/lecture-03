<!DOCTYPE html>
<?php

$variable = "a variable";

$concat = 'concatinate strings'.' '.'with a period';

$outer_double_quotes = "when double quotes are on the outside 'single quotes' are available inside

  multiline strings are avilable

  and \"double quotes\" can be escaped
";

$inner_double_quotes = 'when single quotes are on the outside "double quotes" are available inside
  multiline strings are available

  and \'single quotes\' can be escaped 
';

$interpolate = "you may interpolate $variable in double quotes";
?>
<html>
  <head>
    <title>PHP strings</title>
  </head>
  <body>
    <h3><?= $variable ?></h3>
    <h3><?= $concat ?></h3>
    <h3><?= $outer_double_quotes ?></h3>
    <h3><?= $inner_double_quotes ?></h3>
  </body>
</html>


