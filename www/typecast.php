<!DOCTYPE html>
<?php
  
  $auto_typecast = "5"+25;

  $add_num_strings = "25"+"2";

  $add_non_num = "25"-"asdf";
  
?>
<html>
  <head>
    <title>PHP typecasting</title>
  </head>
  <body>
    <h3><?= $auto_typecast ?></h3>
    <h3><?= $add_num_strings ?></h3>
    <h3><?= $add_non_num ?></h3>
  </body>
</html>


