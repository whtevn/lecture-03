<?php

/*
  PUT ALL NON-DISPLAY PHP LOGIC
  BEFORE THE DOCTYPE DECLARATION
  THIS IS NOT A REQUIREMENT,
  IT IS A SUGGESTION FOR YOUR OWN SANITY 
*/

// define a function with one argument
function add_five($num){
  $sum = $num + 5;  // all lines must end in a semi-colon (;)
  return $sum;      // functions that do not have a return will return undefined
}


$added_num = add_five(20); // use the function

?><!DOCTYPE html>
<html>
  <head>
    <title>functions and variables</title>
  </head>
  <body>
    <h1>The number was <?= $added_num ?></h1>
  </body>
</html>
