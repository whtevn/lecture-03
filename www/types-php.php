<!DOCTYPE html>
<?php

$string = "this is a string";
$num    = 50;

function add_five($num){
  $sum = $num + 5;  // all lines must end in a semi-colon (;)
  return $sum;     // functions that do not have a return will return undefined
}

$added_num = add_five(20); // use the function

$num_type = gettype($added_num);

$updated_num = (string)$added_num;
$updated_num_type = gettype($updated_num);
?>
<html>
  <head>
    <title>PHP Basics</title>
  </head>
  <body>
    <h1>The number was <?= $added_num ?> and type <?= $num_type ?></h1>
    <h2>The number was changed to <?= $updated_num ?> and type <?= $updated_num_type ?></h1>
  </body>
</html>

