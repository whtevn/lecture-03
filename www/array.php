<!DOCTYPE html>
<?php

  // define an array with two elements in it
  $names = ["Jerry", "Janie"];  

  // shorthand way to add an element to an array
  $names[] = "Jethro";

  // longhand way to add an element to an array
  array_push($names, "Jillian");
  
?>
<html>
  <head>
    <title>PHP typecasting</title>
  </head>
  <body>
    <ul>
      <?php foreach($names as $name){ ?>
        <li><?= $name ?></li>
      <?php } ?>
    </ul>
  </body>
</html>
